#ifndef CLASSA_H
#define CLASSA_H

//Forward delcarations
class TTree;
class classB;

//______________________________________________________________
class classA : public TObject {

 private:
  Long64_t start;
  Long64_t stop;
  Double_t xVal;
  Double_t yVal;
  Double_t zVal;
  TTree *childTree; //! //pointer to child tree
  classB *child;    //! //pointer to child object
  
 public:
  classA();
  virtual ~classA();

  void SetX(Double_t val){xVal=val;}
  void SetY(Double_t val){yVal=val;}
  void SetZ(Double_t val){zVal=val;}
  void SetStart(Long64_t val){start=val;}
  void SetStop(Long64_t val){stop=val;}
  void SetChildTree(TTree *val);
  void PrintChildren();

  Long64_t GetNChildren();
  Double_t GetX(){return xVal;}
  Double_t GetY(){return yVal;}
  Double_t GetZ(){return zVal;}
  Double_t GetStop(){return stop;}
  Double_t GetStart(){return start;}
  classB *GetChild(Int_t iChild);

  
  ClassDef(classA,1);
};

#endif
