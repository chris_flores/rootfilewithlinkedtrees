#ifndef CLASSB_H
#define CLASSB_H

//______________________________________________________
class classB : public TObject {
  
private:
  Double_t xVal;
  Double_t yVal;
  Double_t zVal;
  Long64_t parent;
  
public:
  classB();
  virtual ~classB();

  void SetX(Double_t val){xVal=val;}
  void SetY(Double_t val){yVal=val;}
  void SetZ(Double_t val){zVal=val;}
  void SetParent(Long64_t val){parent=val;}
  Double_t GetX(){return xVal;}
  Double_t GetY(){return yVal;}
  Double_t GetZ(){return zVal;}
  Long64_t GetParent(){return parent;}

  ClassDef(classB,1);
};


#endif
