#include <iostream>

#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>

#include <../inc/classB.h>
#include <../inc/classA.h>

void treeReader(TString fileName){

  
  TFile *inFile = new TFile(fileName);

  classA *a = NULL;
  classB *b = NULL;

  TTree *treeA = (TTree *)inFile->Get("treeA");
  TTree *treeB = (TTree *)inFile->Get("treeB");

  treeA->SetBranchAddress("classA",&a);
  a->SetChildTree(treeB);
  
  Int_t nEntries = treeA->GetEntries();
  cout <<"Number of Entries in TreeA: "<<nEntries <<endl;
  
  for (Int_t iEntry=0; iEntry < nEntries; iEntry++){

    treeA->GetEntry(iEntry);
          
    for (Int_t iChild=0; iChild < a->GetNChildren(); iChild++){

      b = a->GetChild(iChild);
      
      cout <<iEntry <<" " <<b->GetParent() <<" "
	   <<iChild <<" " <<a->GetNChildren() <<endl;
      
    }  //End Loop Over Children  
    
    
  }//End Loop Over Entries

  inFile->Close();
  
}
