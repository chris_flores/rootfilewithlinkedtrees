#include <iostream>

#include <TTree.h>

#include "classB.h"
#include "classA.h"

ClassImp(classA);

//_______________________________________________________
classA::classA(){

  xVal=yVal=zVal=start=stop=0;
  childTree == NULL;
  
}

//_______________________________________________________
classA::~classA(){

}

//_______________________________________________________
Long64_t classA::GetNChildren(){
  return stop-start;  
}


//_______________________________________________________
void classA::SetChildTree(TTree *val){
  childTree=val;
  child = NULL;
  childTree->SetBranchAddress("classB",&child);
}

//_______________________________________________________
void classA::PrintChildren(){

  if (!childTree){
    cout <<"ERROR: No Child Tree Connected" <<endl;
    return;
  }

  for (Int_t iStart=start; iStart<=stop; iStart++){

    childTree->GetEntry(iStart);
    
    cout <<child->xVal <<" " <<child->yVal
	 <<child->yVal <<" " <<child->parent <<endl;
  }
  
}

//________________________________________________________
classB *classA::GetChild(Int_t iChild){

  childTree->GetEntry(start+iChild);
  
  return child;

}
