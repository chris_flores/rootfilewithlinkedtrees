#include <iostream>

#include <TRandom3.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>

#include <../inc/classB.h>
#include <../inc/classA.h>


void treeMaker(TString fileName){

  TRandom3 rand(0);

  TFile *outFile = new TFile(fileName,"UPDATE");
  
  classA a;
  classB b;
  
  TTree *treeA = new TTree("treeA","A");
  TBranch *branchA = treeA->Branch("classA", &a, 100000);

  TTree *treeB = new TTree("treeB","B");
  TBranch *branchB = treeB->Branch("classB", &b, 100000);

  Long64_t totalA(0);
  Long64_t totalB(0);
  
  for (Int_t iA=0; iA<100; iA++){

    a.SetX(rand.Uniform(-1,1));
    a.SetY(rand.Uniform(-1,1));
    a.SetZ(rand.Uniform(-1,1));
    a.SetStart(totalB);
    
    for (Int_t iB=0; iB<(Int_t)rand.Uniform(1,10); iB++){
      
      b.SetX(rand.Uniform(-1,1));
      b.SetY(rand.Uniform(-1,1));
      b.SetZ(rand.Uniform(-1,1));
      b.SetParent(totalA);
      
      treeB->Fill();
      
      totalB++;
    }//End Loop Over B
    
    a.SetStop(totalB);

    treeA->Fill();
    
    totalA++;

    
  }//End Loop Over A

  cout <<totalA <<" " <<totalB <<endl;

  treeA->Write();
  treeB->Write();
  outFile->Close();
  
}
